import React from "react";
import { PortfolioPage } from "./pages/PortfolioPage";
import { CreatePage } from "./pages/CreatePage";
import { DetailPage } from "./pages/DetailPage";
import { MainPage } from "./pages/MainPage";
import { AuthPage } from "./pages/AuthPage";
import { Switch, Route, Redirect } from "react-router-dom";
import { ContactsPage } from "./pages/ContactsPage";
import { RecordPage } from "./pages/RecordPage";
import { PricePage } from "./pages/PricePage";

export const useRoutes = isAuthenticated => {
  return (
    <Switch>
      <Route path="/contacts" exact>
        <ContactsPage />
      </Route>
      <Route path="/portfolio" exact>
        <PortfolioPage />
      </Route>
      <Route path="/price" exact>
        <PricePage />
      </Route>
      <Route path="/record" exact>
        <RecordPage />
      </Route>
      <Route path="/" exact>
        <MainPage />
      </Route>
      {/* <Redirect to="/" /> */}
    </Switch>
  );

  if (isAuthenticated) {
    return (
      <Switch>
        <Route path="/links" exact></Route>
        <Route path="/create" exact>
          <CreatePage />
        </Route>
        <Route path="/detail/:id">
          <DetailPage />
        </Route>
        <Redirect to="/" />
      </Switch>
    );
  }
};
