import React from "react";

import { BrowserRouter } from "react-router-dom";
import { useRoutes } from "./routes";
import { useAuth } from "./hooks/Auth.hook";
import "materialize-css";
// import 'bootstrap/dist/css/bootstrap.min.css';
import { AuthContext } from "./context/AuthContext";
import { Navbar } from "./components/Navbar";
import { Loader } from "./components/Loader";

function App() {
  const { token, login, logout, userId, ready } = useAuth();
  const isAuthenticated = !!token;
  const routes = useRoutes(isAuthenticated);

  if (!ready) {
    return <Loader />;
  }
  return (
    <AuthContext.Provider
      value={{ token, login, logout, userId, isAuthenticated }}
    >
      <BrowserRouter>
        {/* {isAuthenticated && <Navbar />} */}
        <Navbar />
        <div className="container">
          <h1>{routes}</h1>
        </div>
      </BrowserRouter>
    </AuthContext.Provider>
  );
}

export default App;
