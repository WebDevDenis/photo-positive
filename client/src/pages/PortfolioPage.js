import React, { useEffect, useState } from "react";

import { Fancybox as NativeFancybox } from "@fancyapps/ui/dist/fancybox.esm.js";
import "@fancyapps/ui/dist/fancybox.css";

function Fancybox(props) {
  const delegate = props.delegate || "[data-fancybox]";

  useEffect(() => {
    const opts = props.options || {};

    NativeFancybox.bind(delegate, opts);

    return () => {
      NativeFancybox.destroy();
    };
  }, []);

  return <>{props.children}</>;
}

const tabItems = [
  {
    id: 1,
    title: "Индивидуальная фотосессия",
    icon: "",
    content: (
      <Fancybox>
        <div className="row">
          <div className="col s-12 m6 l4">
            <a className="p0" data-fancybox="gallery" href="/img/1-min.jpg">
              <img className="w100" alt="1" src="/img/1m-min.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/2-min.jpg">
              <img className="w100" alt="1" src="/img/2m-min.jpg" />
            </a>
          </div>
          <div className="col  s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/3-min.jpg">
              <img className="w100" alt="1" src="/img/3m-min.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/4-min.jpg">
              <img className="w100" alt="1" src="/img/4m-min.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/5-min.jpg">
              <img className="w100" alt="1" src="/img/5m-min.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/6-min.jpg">
              <img className="w100" alt="1" src="/img/6m-min.jpg" />
            </a>
          </div>
        </div>
      </Fancybox>
    )
  },
  {
    id: 2,
    title: "Детская фотосессия",
    icon: "",
    content: (
      <Fancybox>
        <div className="row">
          <div className="col s-12 m6 l4">
            <a className="p0" data-fancybox="gallery" href="/img/d01-min.jpg">
              <img className="w100" alt="1" src="/img/d01m-min.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/d02-min.jpg">
              <img className="w100" alt="1" src="/img/d02m-min.jpg" />
            </a>
          </div>
          <div className="col  s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/d03-min.jpg">
              <img className="w100" alt="1" src="/img/d03m-min.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/d04-min.jpg">
              <img className="w100" alt="1" src="/img/d04m-min.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/d05-min.jpg">
              <img className="w100" alt="1" src="/img/d05m-min.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/d06-min.jpg">
              <img className="w100" alt="1" src="/img/d06m-min.jpg" />
            </a>
          </div>
        </div>
      </Fancybox>
    )
  },
  {
    id: 3,
    title: "Семейная фотосессия",
    icon: "",
    content: (
      <Fancybox>
        <div className="row">
          <div className="col s-12 m6 l4">
            <a className="p0" data-fancybox="gallery" href="/img/se003.jpg">
              <img className="w100" alt="1" src="/img/se003m.jpg" />
            </a>
          </div>
        </div>
      </Fancybox>
    )
  },
  {
    id: 4,
    title: "Фотокнига 9 класс",
    icon: "",
    content: (
      <Fancybox>
        <div className="row">
          <div className="col s-12 m6 l4">
            <a className="p0" data-fancybox="gallery" href="/img/f00.jpg">
              <img className="w100" alt="1" src="/img/f00m.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/f01.jpg">
              <img className="w100" alt="1" src="/img/f01m.jpg" />
            </a>
          </div>
          <div className="col  s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/f02.jpg">
              <img className="w100" alt="1" src="/img/f02m.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/f03.jpg">
              <img className="w100" alt="1" src="/img/f03m.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/f04.jpg">
              <img className="w100" alt="1" src="/img/f04m.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/f05.jpg">
              <img className="w100" alt="1" src="/img/f05m.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/f009.jpg">
              <img className="w100" alt="1" src="/img/f009m.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/f010.jpg">
              <img className="w100" alt="1" src="/img/f010m.jpg" />
            </a>
          </div>
        </div>
      </Fancybox>
    )
  },
  {
    id: 5,
    title: "Детский выпускной альбом",
    icon: "",
    content: (
      <Fancybox>
        <div className="row">
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/d00.jpg">
              <img className="w100" alt="1" src="/img/d00m.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/d001.jpg">
              <img className="w100" alt="1" src="/img/d001m.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/d002.jpg">
              <img className="w100" alt="1" src="/img/d002m.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/d003.jpg">
              <img className="w100" alt="1" src="/img/d003m.jpg" />
            </a>
          </div>
        </div>
      </Fancybox>
    )
  },
  {
    id: 6,
    title: "Альбом начальной школы",
    icon: "",
    content: (
      <Fancybox>
        <div className="row">
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/sc00.jpg">
              <img className="w100" alt="1" src="/img/sc00m.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/sc01.jpg">
              <img className="w100" alt="1" src="/img/sc01m.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/sc002.jpg">
              <img className="w100" alt="1" src="/img/sc002m.jpg" />
            </a>
          </div>
        </div>
      </Fancybox>
    )
  }
];

const TabItemComponent = ({
  icon = "",
  title = "",
  onItemClicked = () => console.error("You passed no action to the component"),
  isActive = false
}) => {
  return (
    <div
      className={isActive ? "collection-item active" : "collection-item"}
      onClick={onItemClicked}
    >
      <i className={icon}></i>
      <p className="font16 m0">{title}</p>
    </div>
  );
};

export const PortfolioPage = () => {
  const [active, setActive] = useState(1);
  return (
    <div>
      <div className="row mflex-block">
        <div class="col s12 l9 block1">
          <div className="content">
            {tabItems.map(({ id, content }) => {
              return active === id ? content : "";
            })}
          </div>
        </div>
        <div className="col s12 l3 block2">
          <div className=" collection">
            {tabItems.map(({ id, icon, title }) => (
              <TabItemComponent
                key={title}
                icon={icon}
                title={title}
                onItemClicked={() => setActive(id)}
                isActive={active === id}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};
