import React, { useState, useEffect } from "react";
import Calendar from "react-calendar";

import { Fancybox as NativeFancybox } from "@fancyapps/ui/dist/fancybox.esm.js";
import "@fancyapps/ui/dist/fancybox.css";

function Fancybox(props) {
  const delegate = props.delegate || "[data-fancybox]";

  useEffect(() => {
    const opts = props.options || {};

    NativeFancybox.bind(delegate, opts);

    return () => {
      NativeFancybox.destroy();
    };
  }, []);

  return <>{props.children}</>;
}
// import "react-calendar/dist/Calendar.css";
export const PricePage = () => {
  const [value, onChange] = useState(new Date());
  return (
    <div>
      <h1>Цены</h1>

      <Fancybox>
        <div className="row">
          <div className="col s-12 m6 l4">
            <a className="p0" data-fancybox="gallery" href="/img/111.jpg">
              <img className="w100" alt="1" src="/img/111.jpg" />
            </a>
          </div>
          <div className="col s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/222.jpg">
              <img className="w100" alt="1" src="/img/222.jpg" />
            </a>
          </div>
          <div className="col  s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/333.jpg">
              <img className="w100" alt="1" src="/img/333.jpg" />
            </a>
          </div>
          <div className="col  s-12 m6  l4">
            <a className="p0" data-fancybox="gallery" href="/img/444.jpg">
              <img className="w100" alt="1" src="/img/444.jpg" />
            </a>
          </div>
        </div>
      </Fancybox>
    </div>
  );
};
