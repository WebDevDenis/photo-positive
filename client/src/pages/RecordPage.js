import React, { useState } from "react";
import Calendar from "react-calendar";
// import "react-calendar/dist/Calendar.css";
export const RecordPage = () => {
  const [value, onChange] = useState(new Date());
  return (
    <div>
      <h1> Запись</h1>
      <Calendar onChange={onChange} value={value} />
    </div>
  );
};
