import React from "react";

export const ContactsPage = () => {
  return (
    <div>
      <h1> Контакты</h1>
      <p>
        Наш инстаграмм -{" "}
        <a
          href="https://www.instagram.com/photo_positive38/"
          style={{ color: "#fbba05", padding: "5px" }}
        >
          @photo_positive38
        </a>
      </p>
      <p>📍Мы находимся по адресу Суворова д.2 (остановка военкомат)</p>
      <p>☎️Записаться можно в директ или по номеру телефона:89025760344</p>
      <iframe
        src="https://yandex.ru/map-widget/v1/?um=constructor%3A2c7276552f305b33ddc42e38edd831aed8294f6a95ed99203c8d0b086fed3b50&amp;source=constructor"
        width="100%"
        height="720"
        frameborder="0"
      ></iframe>
    </div>
  );
};
