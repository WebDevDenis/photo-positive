import { React, useEffect } from "react";
import { NavLink, useHistory } from "react-router-dom";
import { AuthContext } from "../context/AuthContext";
import { useContext } from "react";

import { ReactComponent as Logo } from "../logo.svg";

export const Navbar = () => {
  const history = useHistory();
  useEffect(() => {
    let sidenav = document.querySelector("#mobile-demo");
    window.M.Sidenav.init(sidenav, {});
  }, []);
  const closeHandler = event => {
    console.log(event.target);
    // event.preventDefault();

    document.querySelector(".sidenav-overlay").click();
    // let instance = window.M.Sidenav.close();
  };
  const auth = useContext(AuthContext);
  const logoutHandler = event => {
    event.preventDefault();
    auth.logout();
    history.push("/");
  };
  return (
    <>
      <div className="container">
        <div>
          <nav>
            <div class="nav-wrapper">
              <a href="/" className="brand-logo">
                <Logo className="logo" />
              </a>

              <a href="#" data-target="mobile-demo" class="sidenav-trigger">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="48"
                  height="48"
                  viewBox="0 0 48 48"
                  fill="#fbba05"
                >
                  <rect
                    width="48"
                    height="48"
                    fill="#fbba05"
                    fill-opacity="1"
                  />
                  <path
                    d="M7.94977 11.9498H39.9498"
                    stroke="black"
                    stroke-width="4"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                  <path
                    d="M7.94977 23.9498H39.9498"
                    stroke="black"
                    stroke-width="4"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                  <path
                    d="M7.94977 35.9498H39.9498"
                    stroke="black"
                    stroke-width="4"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                </svg>
              </a>
              <ul class="right hide-on-med-and-down">
                <li>
                  <NavLink to="/" style={{ "white-space": "nowrap" }}>
                    Главная
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/portfolio">Портфолио</NavLink>
                </li>
                <li>
                  <NavLink to="/contacts">Контакты</NavLink>
                </li>
                <li>
                  <NavLink to="/price">Цены</NavLink>
                </li>
                {/* <li> */}
                {/* <NavLink to="/record" >Календарь</NavLink> */}
                {/* </li> */}
              </ul>
            </div>
          </nav>
          {/* 
        <a href="/" onClick={logoutHandler}>
          Выйти
        </a> */}
        </div>
      </div>

      <ul class="sidenav" id="mobile-demo">
        <li>
          <a href="/" className="brand-logo">
            <Logo className="logo" />
          </a>
        </li>
        <li>
          <NavLink
            onClick={closeHandler}
            to="/"
            style={{ "white-space": "nowrap" }}
          >
            О студии
          </NavLink>
        </li>
        <li>
          <NavLink onClick={closeHandler} to="/portfolio">
            Портфолио
          </NavLink>
        </li>
        <li>
          <NavLink onClick={closeHandler} to="/contacts">
            Контакты
          </NavLink>
        </li>
        <li>
          <NavLink onClick={closeHandler} to="/price">
            Цены
          </NavLink>
        </li>
      </ul>
    </>
  );
};
