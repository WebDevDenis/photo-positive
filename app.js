const express = require("express");
const config = require("config");
const fs = require("fs");
const http = require("http");
const https = require("https");
const mongoose = require("mongoose");
const path = require("path");

const app = express();
app.enable("trust proxy");
app.use(function(request, response, next) {
  if (process.env.NODE_ENV != "development" && !request.secure) {
    return response.redirect("https://" + request.headers.host + request.url);
  }

  next();
});
const privateKey = fs.readFileSync(
  "/etc/letsencrypt/live/photo-positive.ru/privkey.pem",
  "utf8"
);
const certificate = fs.readFileSync(
  "/etc/letsencrypt/live/photo-positive.ru/cert.pem",
  "utf8"
);
const ca = fs.readFileSync(
  "/etc/letsencrypt/live/photo-positive.ru/chain.pem",
  "utf8"
);

const credentials = {
  key: privateKey,
  cert: certificate,
  ca: ca
};

app.use(express.json({ extended: true }));

app.use("/api/auth", require("./routes/auth.routers"));
app.use("/api/link", require("./routes/link.routers"));

if (process.env.NODE_ENV === "production") {
  app.use("/", express.static(path.join(__dirname, "client", "build")));
  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}
const PORT = config.get("port") || 5000;

async function start() {
  try {
    await mongoose.connect(config.get("mongoUri"), {
      useNewUrlParser: true,
      useUnifiedTopology: true
      // useCreateIndex:true
    });
  } catch (e) {
    console.log("Server error", e.message);
    process.exit(1);
  }
}

start();

// app.listen(PORT, () => console.log(`app ${PORT}`));

// Starting both http & https servers
const httpServer = http.createServer(app);
const httpsServer = https.createServer(credentials, app);

httpServer.listen(80, () => {
  console.log("HTTP Server running on port 80");
});

httpsServer.listen(443, () => {
  console.log("HTTPS Server running on port 443");
});
